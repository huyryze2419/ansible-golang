package movies

import (
	"fmt"
	tmdb "github.com/cyruzin/golang-tmdb"
	"github.com/gofiber/fiber/v2"
	"strconv"
)

func GetBook(ctx *fiber.Ctx) error {
	tmdbClient, err := tmdb.Init("5a07fddb6f1f59e13217473da206d6b9")

	if err != nil {
		fmt.Println(err)
	}
	id, _ := strconv.Atoi(ctx.Params("id"))
	movie, err := tmdbClient.GetMovieDetails(id, nil)

	if err != nil {
		fmt.Println(err)
	}

	return ctx.JSON(fiber.Map{
		"error": false,
		"msg":   nil,
		"movie": movie,
	})

}
