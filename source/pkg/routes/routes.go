package routes

import (
	"github.com/gofiber/fiber/v2"
	"source/pkg/controllers/movies"
)

func PublicRoutes(a *fiber.App) {
	// Create routes group.
	route := a.Group("/api/v1")

	// Routes for GET method:
	route.Get("/movie/:id", movies.GetBook) // delete one book by ID
}
