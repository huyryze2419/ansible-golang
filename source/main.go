package main

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"source/pkg/configs"
	"source/pkg/routes"
)

func main() {
	// Define Fiber config.
	config := configs.FiberConfig()

	// Define a new Fiber app with config.
	app := fiber.New(config)

	routes.PublicRoutes(app) // Register a public routes for app.

	// Start server (with or without graceful shutdown).
	err := app.Listen(":3000")
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("server running http://localhost:3000")
	}
}
